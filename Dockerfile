
## docker build -t gitlab-registry.cern.ch/swan/docker-images/dev-jupyter .
## docker push gitlab-registry.cern.ch/swan/docker-images/dev-jupyter

FROM jupyter/minimal-notebook:63d0df23b673


MAINTAINER Diogo Castro <diogo.castro@cern.ch>
MAINTAINER Piotr Mrowczynski <piotr.mrowczynski@cern.ch>

USER root

RUN apt-get update

# Install required apt packages for all the extensions
RUN apt-get -y install \
        krb5-user git curl openjdk-8-jdk

# Install required python packages for all the extensions
# Force lab version 2, since we're targeting development to that version
RUN pip install \
        pyspark \
        jupyter_nbextensions_configurator \
        bs4 \
        kubernetes==9.0.0 \
        'jupyterlab>=2.0.0rc2'

# Install less and yarn in case one wants to build JavaScript inside the docker entrypoint
RUN npm install less@2.7.1 -g && \
    npm install -g less-plugin-clean-css && \
    npm install -g yarn

# Use bash instead of dash
RUN rm /bin/sh && \
    ln -s /bin/bash /bin/sh

ARG URL_NBEXTENSIONS=https://gitlab.cern.ch/api/v4/projects/25624/jobs/artifacts/qa/download?job=release-daily
ARG SWAN_COMMON_BRANCH=qa

# Get latest - not necessarily tagged - version of the css files and compile them
RUN git clone -b $SWAN_COMMON_BRANCH https://gitlab.cern.ch/swan/common.git /home/$NB_USER/.jupyter/custom && \
    lessc --clean-css /home/$NB_USER/.jupyter/custom/css/style.less /home/$NB_USER/.jupyter/custom/css/style.css

# Install our extensions just like in prod (but also enable sparkconnector and hdfsbrowser)
RUN mkdir /tmp/jupyter_extensions && \
    cd /tmp/jupyter_extensions && \
    wget ${URL_NBEXTENSIONS} -O extensions.zip && \
    unzip extensions.zip && \
    rm -rf /opt/conda/lib/python3.7/site-packages/notebook/templates/ && \
    mv -f templates /opt/conda/lib/python3.7/site-packages/notebook/ && \
    # Install all SWAN extensions which are packaged as python modules
    # Ignore dependencies because they have already been installed or come from CVMFS
    ls -d ./*/ | xargs -n1 sh -c 'cd $0 ; pip install --no-deps .' && \
    # Automatically install all nbextensions from their python module (all extensions need to implement the api even if they return 0 nbextensions)
    ls -d ./*/ | xargs -n1 sh -c 'extension=$(basename $0) ; jupyter nbextension install --py --system ${extension,,} || exit 1' && \
    # Enable the server extensions
    server_extensions=('swancontents' 'sparkmonitor' 'swannotebookviewer' 'swangallery' 'sparkconnector' 'hdfsbrowser') && \
    for extension in ${server_extensions[@]}; do jupyter serverextension enable --py --system $extension || exit 1 ; done && \
    # Enable the nb extensions
    # Not all nbextensions are activated as some of them are activated on session startup or by the import in the templates
    nb_extensions=('swanhelp' 'swannotifications' 'swanshare' 'swanintro' 'sparkmonitor' 'sparkconnector' 'hdfsbrowser') && \
    for extension in ${nb_extensions[@]}; do jupyter nbextension enable --py --system $extension || exit 1; done && \
    # Force nbextension_configurator systemwide to prevent users disabling it
    jupyter nbextensions_configurator enable --system && \
    # Clean
    rm -rf /tmp/jupyter_extensions

# Enable Kernel extensions
RUN mkdir -p /home/$NB_USER/.ipython/profile_default/ && \
    printf "c.InteractiveShellApp.extensions.append('sparkmonitor.kernelextension') \
\nc.InteractiveShellApp.extensions.append('sparkconnector.connector') \
\nc.InteractiveShellApp.extensions.append('swankernelenv') \
" > /home/$NB_USER/.ipython/profile_default/ipython_kernel_config.py

# Set Jupyter configurations
RUN printf "c.NotebookApp.default_url = 'projects' \
\nc.NotebookApp.contents_manager_class = 'swancontents.filemanager.swanfilemanager.SwanFileManager' \
\nc.ContentsManager.checkpoints_class = 'swancontents.filemanager.checkpoints.EOSCheckpoints' \
\nc.NotebookApp.token = '' \
" > /home/$NB_USER/.jupyter/jupyter_notebook_config.py

# Adjust Permissions to be able to overwrite the files
RUN chmod g+w -R /usr/local/share/jupyter/nbextensions && \
    chown -R root:users /usr/local/share/jupyter/nbextensions && \
    chmod g+w -R /opt/conda/lib/python3.7/site-packages

# Add projects folder and fix permissions
RUN mkdir /home/$NB_USER/SWAN_projects && \
    fix-permissions /home/$NB_USER

USER $NB_UID