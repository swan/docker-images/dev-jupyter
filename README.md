# dev-jupyter

Docker image to develop Jupyter and Jupyterlab extensions locally

## Run the image

```bash
docker-compose up
```

Local extensions can be mounted via volumes inside the container and installed in `entrypoint`.

To access the container, just go to `http://localhost:8888/` or `http://localhost:8888/lab` for Jupyterlab.

To test Jupyterlab extensions, it's recommended to:

1. Open Jupyterlab, create 2 tabs in the browser, one with terminal, one to test your extension.
1. Execute all the required build commands in jupyterlab terminal, in second window jupyterlab will ask you to reload when ready.

## Build and push a new image

The extensions might be outdated, so is recommended to build a new image every now and then.

```bash
docker build --no-cache -t gitlab-registry.cern.ch/swan/docker-images/dev-jupyter .
docker push gitlab-registry.cern.ch/swan/docker-images/dev-jupyter
```